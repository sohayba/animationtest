import { Body, Container, Content, Header, Icon, Left, Right } from 'native-base';
import React, { Component } from 'react';
import { Animated, Dimensions, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import Button from './Components/Button';
import { Assets } from './Utilities/Assets';

// The position of the header element on the start of the animation
const HEADER_ELEM_START_POSITION = 20;
// The target position of the header element after the end of the animation
const HEADER_ELEM_END_POSITION = 0;

// The opacity of the header element on the start of the animation
const HEADER_ELEM_START_OPACITY = 0;
// The target opacity of the header element after the animation
const HEADER_ELEM_END_OPACITY = 1;

// The opacity of the main section on the start of the animation
const MAIN_SECTION_START_OPACITY = 1;

// The target opacity of the main section after the end of the animation
const MAIN_SECTION_END_OPACITY = 0;

// The duration of the header animation (in milliseconds)
const HEADER_ANIMATION_DURATION = 500;

// The y position of the scroll to start the animation (show/hide the elements)
const HEADER_ANIMATION_SCROLL_POSITION = 90;

export default class App extends Component {

    isHeaderVisible = false;
    headerElemPositionAnimation = new Animated.Value(HEADER_ELEM_START_POSITION);
    headerElemOpacityAnimation = new Animated.Value(HEADER_ELEM_START_OPACITY);
    mainSectionAnimation = new Animated.Value(MAIN_SECTION_START_OPACITY)

    constructor(props) {
        super(props);
        this.state = {
            isHeaderScrolled: false
        }
    }

    /**
     * Starts the show animation
     * The animation will show the header elements (body and left)
     * and hides the main section of the screen
     */
    showHeaderElems = () => {
        Animated.timing(
            this.headerElemPositionAnimation,
            {
                toValue: HEADER_ELEM_END_POSITION,
                duration: HEADER_ANIMATION_DURATION,
                useNativeDriver: false
            }
        ).start();
        Animated.timing(
            this.headerElemOpacityAnimation,
            {
                toValue: HEADER_ELEM_END_OPACITY,
                duration: HEADER_ANIMATION_DURATION,
                useNativeDriver: false
            }
        ).start();
        Animated.timing(
            this.mainSectionAnimation,
            {
                toValue: MAIN_SECTION_END_OPACITY,
                duration: HEADER_ANIMATION_DURATION,
                useNativeDriver: false
            }
        ).start();
        this.isHeaderVisible = true;
        if (this.props.onShowHeader) {
            this.props.onShowHeader();
        }
    }

    /**
     * Starts the hide animation
     * The animation will hide the header elements (body and left)
     * and shows the main section of the screen
     */
    hideHeaderElems = () => {
        Animated.timing(
            this.headerElemPositionAnimation,
            {
                toValue: HEADER_ELEM_START_POSITION,
                duration: HEADER_ANIMATION_DURATION,
                useNativeDriver: false
            }
        ).start();
        Animated.timing(
            this.headerElemOpacityAnimation,
            {
                toValue: HEADER_ELEM_START_OPACITY,
                duration: HEADER_ANIMATION_DURATION,
                useNativeDriver: false
            }
        ).start();
        Animated.timing(
            this.mainSectionAnimation,
            {
                toValue: MAIN_SECTION_START_OPACITY,
                duration: HEADER_ANIMATION_DURATION,
                useNativeDriver: false
            }
        ).start();
        this.isHeaderVisible = false;
        if (this.props.onHideHeader) {
            this.props.onHideHeader();
        }
    }

    /**
     * Handles the event when the main content is scrolled.
     * @param {*} event 
     */
    handleScroll = (event) => {
        let currentYPosition = event.nativeEvent.contentOffset.y;
        if (this.isHeaderVisible) {
            // Header is visible, check for position to show the header
            if (currentYPosition < HEADER_ANIMATION_SCROLL_POSITION) {
                this.hideHeaderElems();
            }
        } else {
            // Header is hidden, check for position to hide the header
            if (currentYPosition > HEADER_ANIMATION_SCROLL_POSITION) {
                this.showHeaderElems();
            }
        }

        if (this.state.isHeaderScrolled) {
            if (currentYPosition < 10) {
                this.setState({
                    isHeaderScrolled: false
                })
            }
        } else {
            if (currentYPosition > 10) {
                this.setState({
                    isHeaderScrolled: true
                })
            }
        }
    }

    render() {
        return (
            <Container>
                <Header transparent style={this.state.isHeaderScrolled ?
                    styles.scrolledHeader : styles.unScrolledHeader}>
                    <Left style={{
                        flex: 1
                    }}>
                        <Button icon="chevron-back" text="Apps" />
                    </Left>
                    <Body style={{
                        flex: 1,
                        alignItems: 'center'
                    }}>
                        <Animated.View style={{
                            opacity: this.headerElemOpacityAnimation,
                            marginTop: this.headerElemPositionAnimation
                        }}>
                            <Image source={Assets.APP_LOGO} style={styles.headerCenterLogo} />
                        </Animated.View>
                    </Body>
                    <Right style={{
                        flex: 1
                    }}>
                        <Animated.View style={{
                            opacity: this.headerElemOpacityAnimation,
                            marginTop: this.headerElemPositionAnimation
                        }}>
                            <Button icon="cloud-download-outline" />
                        </Animated.View>
                    </Right>
                </Header>
                <Content onScroll={this.handleScroll}>
                    <Animated.View style={{
                        padding: 20,
                        flexDirection: 'row',
                        opacity: this.mainSectionAnimation
                    }}>
                        <View style={{
                            marginRight: 10
                        }}>
                            <Image source={Assets.APP_LOGO} style={{
                                width: 100,
                                height: 100,
                                borderRadius: 30
                            }} />
                        </View>
                        <View style={styles.appInfo}>
                            <Text style={styles.primaryAppName} textBreakStrategy="simple">
                                PicsArt Montage Photo & Video
                            </Text>
                            <Text style={styles.secondaryAppDetails}>
                                Edituer Sticker, Meme, Collage
                            </Text>
                            <View style={styles.buttonsContainer}>
                                <Button icon="cloud-download-outline" />
                                <Button icon="share-outline" />
                            </View>
                        </View>
                    </Animated.View>
                    {this.renderScrollPlaceholders()}
                </Content>
            </Container>
        )
    }

    /**
     * Renders some placeholders to fill screen for the scroll to appear
     */
    renderScrollPlaceholders() {
        let items = [];
        for (let i = 0; i < 30; i++) {
            items.push(<Text style={styles.scrollPlaceholderItem}>Scroll Placeholder</Text>)
        }

        return items;
    }
}

const styles = StyleSheet.create({
    scrolledHeader: {
        borderBottomColor: '#e7e7e7',
        borderBottomWidth: 1
    },
    unScrolledHeader: {

    },
    headerCenterLogo: {
        width: 30,
        height: 30,
        borderRadius: 10
    },
    appInfo: {
        flexWrap: 'wrap',
        flex: 1
    },
    primaryAppName: {
        fontSize: 25,
        fontWeight: 'bold',
        flexWrap: 'wrap',
        flex: 1,
        width: Dimensions.get('window').width - 130
    },
    secondaryAppDetails: {
        fontSize: 15,
        color: 'gray',
        flexWrap: 'wrap',
        flex: 1
    },
    buttonsContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: 20
    },
    primaryIcon: {
        color: Colors.primary,
        fontSize: 30
    },
    scrollPlaceholderItem: {
        textAlign: 'center',
        padding: 20
    }
})