import { Icon } from 'native-base';
import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';

/**
 * Custom button component
 * It will recieve the follow props
 * @param {string} icon the icon name from react-native-vector-icons rendered in button
 *                      if passed as null no icon will be rendered
 * @param {string} text the button label to be rendered
 * @param {function} onPress the function to be executed when the button is pressed
 */
export default class Button extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (<TouchableOpacity onPress={() => {
            if (this.props.onPress != null) {
                this.props.onPress();
            }
        }}>
            <View style={styles.btnContainer}>
                {this.props.icon != null && <Icon style={styles.icon} name={this.props.icon} />}
                {this.props.text != null && <Text style={styles.text}>{this.props.text}</Text>}
            </View>
        </TouchableOpacity>)
    }
}

const styles = StyleSheet.create({
    btnContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    icon: {
        color: Colors.primary,
        fontSize: 32
    },
    text: {
        color: Colors.primary,
        fontSize: 18
    }
})